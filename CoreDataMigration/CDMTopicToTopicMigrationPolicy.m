//------------------------------------------------------------------------------
//
//  CDMTopicToTopicMigrationPolicy.m
//  CoreDataMigration
//
//  Created by William Moss on 1/14/14.
//  Copyright (c) 2014 Bill Moss. All rights reserved.
//
//------------------------------------------------------------------------------

#import "CDMTopicToTopicMigrationPolicy.h"
#import "NSManagedObjectModel+ModelPaths.h"

//------------------------------------------------------------------------------

@interface CDMTopicToTopicMigrationPolicy ()

@property (strong, nonatomic) NSString          *destinationModelName;

@end

//------------------------------------------------------------------------------
@implementation CDMTopicToTopicMigrationPolicy
//------------------------------------------------------------------------------

- (BOOL)beginEntityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
    self.destinationModelName = [manager.destinationModel modelName];
    return [super beginEntityMapping:mapping manager:manager error:error];
}


//------------------------------------------------------------------------------

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
    BOOL result = [super createDestinationInstancesForSourceInstance:sInstance entityMapping:mapping manager:manager error:error];
    NSLog(@"Topic - createDestinationInstancesForSourceInstance: %@", [sInstance valueForKey:@"content"]);
    
//    if (nil != error)
//    {
//        NSManagedObject *dInstance = [[manager destinationInstancesForEntityMappingNamed:mapping.name sourceInstances:@[sInstance]] firstObject];
//        
//        // Set timeBudget as minutes...based on length of the topic content.
//        NSString *content = [dInstance valueForKey:@"content"];
//        double aNumMinutes = ceil(content.length * 0.1);
//        NSNumber *timeBudget = [NSNumber numberWithDouble:aNumMinutes];
//        [dInstance setValue:timeBudget forKeyPath:@"timeBudget"];
//    }
    
    return result;
}


//------------------------------------------------------------------------------

- (BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
    BOOL result = [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];
    NSLog(@"Topic - createRelationshipsForDestinationInstance: %@", [dInstance valueForKey:@"content"]);
    return result;
}

//------------------------------------------------------------------------------

- (NSNumber*) timeBudgetForContent:(NSString*)content
{
    if (YES == [self.destinationModelName isEqualToString:@"Model2"])
    {
        double aNumMinutes = ceil(content.length * 0.1);
        NSNumber *timeBudget = [NSNumber numberWithDouble:aNumMinutes];
        return timeBudget;
    }
    else if (YES == [self.destinationModelName isEqualToString:@"Model3"])
    {
        double aNumSeconds = ceil(content.length * 0.1 * 60);
        NSNumber *timeBudget = [NSNumber numberWithDouble:aNumSeconds];
        return timeBudget;
    }
    else
    {
        return nil;
    }
}

//------------------------------------------------------------------------------
@end
//------------------------------------------------------------------------------
