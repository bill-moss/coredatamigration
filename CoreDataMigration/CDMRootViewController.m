//------------------------------------------------------------------------------
//
//  CDMRootViewController.m
//  CoreDataMigration
//
//  Created by Bill Moss on 1/10/14.
//  Copyright (c) 2014 Bill Moss. All rights reserved.
//
//------------------------------------------------------------------------------

#import "CDMRootViewController.h"
#import "CDMModelManager.h"
#import "CDMAppDelegate.h"
#import "CDMTopicListsViewController.h"
#import "CDMModelMigrationManager.h"
#import "NSManagedObjectModel+ModelPaths.h"

//------------------------------------------------------------------------------

@interface CDMRootViewController ()

@property (strong, nonatomic) CDMModelManager       *modelManager;

@property (weak, nonatomic) IBOutlet UIButton       *storeOpenMigrateButton;
@property (weak, nonatomic) IBOutlet UILabel        *useInferredMappingModelLabel;
@property (weak, nonatomic) IBOutlet UISwitch       *useInferredMappingModelSwitch;
@property (weak, nonatomic) IBOutlet UILabel        *storeModelVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel        *appModelVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel        *useProgressiveMigrationLabel;
@property (weak, nonatomic) IBOutlet UISwitch       *useProgressiveMigrationSwitch;
@end

//------------------------------------------------------------------------------
@implementation CDMRootViewController
//------------------------------------------------------------------------------

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

//------------------------------------------------------------------------------

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // Create sample store if we don't have one.
    NSError *error = nil;
    self.modelManager = [CDMModelManager new];
    if (NO == [self.modelManager doesSamplePersistentStoreExist])
    {
        [self.modelManager createSamplePersistentStore:&error];
        if (nil != error)
        {
            [CDMAppDelegate handleApplicationError:error alertUser:YES alertTitle:@"Create Store Failed"];
        }
        else
        {
            [CDMAppDelegate handleApplicationAlertWithTitle:@"Sample Store was Created" message:nil];
            [self.storeOpenMigrateButton setTitle:@"View Store Data" forState:UIControlStateNormal];
            [self.storeOpenMigrateButton addTarget:self action:@selector(handleShowTopicLists) forControlEvents:UIControlEventTouchUpInside];
            [self handleShowTopicLists];
        }
    }
    else if (YES == [self.modelManager isMigrationNeeded])
    {
        NSMappingModel *inferredMappingModel = [self.modelManager inferredMappingModelForMigration];
        BOOL inferredMappingModelAvailable = nil != inferredMappingModel;
        
        self.useInferredMappingModelLabel.hidden = NO;
        self.useInferredMappingModelSwitch.hidden = NO;
        
        self.useInferredMappingModelLabel.enabled = inferredMappingModelAvailable;
        self.useInferredMappingModelSwitch.enabled = inferredMappingModelAvailable;
        self.useInferredMappingModelSwitch.on = NO;
        
        self.useProgressiveMigrationLabel.hidden = NO;
        self.useProgressiveMigrationSwitch.hidden = NO;
        self.useProgressiveMigrationLabel.enabled = YES;
        self.useProgressiveMigrationSwitch.enabled = YES;
        self.useProgressiveMigrationSwitch.on = NO;
        
        BOOL migrationIsPossible = inferredMappingModelAvailable;
        
        if (YES == migrationIsPossible)
        {
            [self.storeOpenMigrateButton setTitle:@"Migrate Perisistent Store" forState:UIControlStateNormal];
            [self.storeOpenMigrateButton addTarget:self action:@selector(handleMigration:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [self.storeOpenMigrateButton setTitle:@"No Migration Option" forState:UIControlStateNormal];
            self.storeOpenMigrateButton.enabled = NO;
        }
    }
    else
    {
        [self.storeOpenMigrateButton setTitle:@"Open Persistent Store" forState:UIControlStateNormal];
        [self.storeOpenMigrateButton addTarget:self action:@selector(handleOpenPersistentStore:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self updateStoreModelVersionLabel];
    
    NSManagedObjectModel *appModel = [self.modelManager managedObjectModel];
    NSSet *versionIdentifers = appModel.versionIdentifiers;
    self.appModelVersionLabel.text = [versionIdentifers.allObjects firstObject];
}

//------------------------------------------------------------------------------

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//------------------------------------------------------------------------------

#pragma mark - Navigation

//------------------------------------------------------------------------------

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (YES == [segue.identifier isEqualToString:@"ShowTopicListsSegue"])
    {
        [self handleShowTopicListsSeque:segue];
    }
}


//------------------------------------------------------------------------------

#pragma mark - Store Handling

//------------------------------------------------------------------------------

- (void) updateStoreModelVersionLabel
{
    NSManagedObjectModel *storeModel = [self.modelManager persistentStoreManagedObjectModel];
    NSSet *versionIdentifers = storeModel.versionIdentifiers;
    self.storeModelVersionLabel.text = [versionIdentifers.allObjects firstObject];
}


//------------------------------------------------------------------------------

- (IBAction) handleMigration:(id)sender
{
    NSLog(@"Migration Started");
    
    NSMutableDictionary *optionsDictionary = nil;
    
    if (YES == self.useProgressiveMigrationSwitch.on)
    {
        NSError *error = nil;
        
        CDMModelMigrationManager *migrationManager = [[CDMModelMigrationManager alloc] init];
        BOOL migrationSucceeded = [migrationManager migratePersistentStoreURL:self.modelManager.persistentStoreURL ofType:self.modelManager.storeType finalDestinationModel:self.modelManager.managedObjectModel error:&error];
        
        if (NO == migrationSucceeded)
        {
            [CDMAppDelegate handleApplicationError:error alertUser:YES alertTitle:@"Migration Failed"];
            return;
        }
    }
    else
    {
        // Options for adding the persistent store to the store coordinator.
        
        // Migrate the store automatically if store model is not the current model in our app.
        optionsDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{NSMigratePersistentStoresAutomaticallyOption: @YES}];
        
        // If Core Data was able to infer the mapping and we want to use that mapping...set the following option.
        if (YES == self.useInferredMappingModelSwitch.on)
        {
            [optionsDictionary setValue:@YES forKey:NSInferMappingModelAutomaticallyOption];
        }
        
        // Note, the combination of NSMigratePersistentStoresAutomaticallyOption = YES and
        // NSInferMappingModelAutomaticallyOption = YES are the settings for a "Lightweight" migration.

        NSLog(@"Migration from %@ to %@ Started", [[self.modelManager persistentStoreManagedObjectModel] modelName], [self.modelManager.managedObjectModel modelName]);
    }
    
    
    // Open the store now...for progressive migration...we dont have migration options.
    NSError *error = nil;
    NSPersistentStore *store = [self.modelManager addPersistentStoreWithOptions:optionsDictionary error:&error];
    if (nil != error)
    {
        [CDMAppDelegate handleApplicationError:error alertUser:YES alertTitle:@"Migration Failed"];
    }
    else if (nil != store)
    {
        [CDMAppDelegate handleApplicationAlertWithTitle:@"Migration Successful" message:nil];
        [self.storeOpenMigrateButton setTitle:@"View Store Data" forState:UIControlStateNormal];
        [self.storeOpenMigrateButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [self.storeOpenMigrateButton addTarget:self action:@selector(handleShowTopicLists) forControlEvents:UIControlEventTouchUpInside];
        [self handleShowTopicLists];
        [self updateStoreModelVersionLabel];
        self.useInferredMappingModelLabel.hidden = YES;
        self.useInferredMappingModelSwitch.hidden = YES;
        self.useProgressiveMigrationLabel.hidden = YES;
        self.useProgressiveMigrationSwitch.hidden = YES;
    }
    else
    {
        [CDMAppDelegate handleApplicationAlertWithTitle:@"Migration Failed" message:@"NSPersistentStore is nil."];
        [self.storeOpenMigrateButton setTitle:@"Migration Failed" forState:UIControlStateNormal];
    }
}

//------------------------------------------------------------------------------

- (IBAction) handleOpenPersistentStore:(id)sender
{
    NSLog(@"Opening Store");
    
    NSError *error = nil;
    NSPersistentStore *store = [self.modelManager addPersistentStoreWithOptions:nil error:&error];
    if (nil != error)
    {
        [CDMAppDelegate handleApplicationError:error alertUser:YES alertTitle:@"Open Store Failed"];
    }
    else if (nil != store)
    {
        [self.storeOpenMigrateButton setTitle:@"View Store Data" forState:UIControlStateNormal];
        [CDMAppDelegate handleApplicationAlertWithTitle:@"Open Store Successful" message:nil];
        [self.storeOpenMigrateButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [self.storeOpenMigrateButton addTarget:self action:@selector(handleShowTopicLists) forControlEvents:UIControlEventTouchUpInside];
        [self handleShowTopicLists];
    }
    else
    {
        [CDMAppDelegate handleApplicationAlertWithTitle:@"Open Store Failed" message:@"NSPersistentStore is nil."];
        [self.storeOpenMigrateButton setTitle:@"Open Store Failed" forState:UIControlStateNormal];
        [self.storeOpenMigrateButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    }
}

//------------------------------------------------------------------------------

- (IBAction) handleMigrationOptionChanged:(id)sender
{
    if (sender == self.useProgressiveMigrationSwitch)
    {
        if (YES == self.useProgressiveMigrationSwitch.on)
        {
            self.useInferredMappingModelSwitch.on = NO;
        }
    }
    else if (sender == self.useInferredMappingModelSwitch)
    {
        if (YES == self.useInferredMappingModelSwitch.on)
        {
            self.useProgressiveMigrationSwitch.on = NO;
        }
    }
}

//------------------------------------------------------------------------------

#pragma mark - Segue Handling

//------------------------------------------------------------------------------

- (void) handleShowTopicLists
{
    [self performSegueWithIdentifier:@"ShowTopicListsSegue" sender:self];
}

//------------------------------------------------------------------------------

- (void) handleShowTopicListsSeque:(UIStoryboardSegue *)segue
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TopicList"];
    NSArray *sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    fetchRequest.sortDescriptors = sortDescriptors;
    
    NSFetchedResultsController *topicListsFetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.modelManager.mainManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    CDMTopicListsViewController *topicListsViewController = (CDMTopicListsViewController*) segue.destinationViewController;
    topicListsViewController.topicListsFetchController = topicListsFetchController;
}

//------------------------------------------------------------------------------
@end
//------------------------------------------------------------------------------
