//------------------------------------------------------------------------------
//
//  CDMTopicsViewController.m
//  CoreDataMigration
//
//  Created by William Moss on 1/15/14.
//  Copyright (c) 2014 Bill Moss. All rights reserved.
//
//------------------------------------------------------------------------------

#import "CDMTopicsViewController.h"
#import "CDMAppDelegate.h"

//------------------------------------------------------------------------------

@interface CDMTopicsViewController ()

@end

//------------------------------------------------------------------------------

@implementation CDMTopicsViewController

//------------------------------------------------------------------------------

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationItem.title = [self.topicList valueForKey:@"title"];
    [self.tableView reloadData];
}

//------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//------------------------------------------------------------------------------

#pragma mark - Navigation

//------------------------------------------------------------------------------

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//}

//------------------------------------------------------------------------------

#pragma mark - UITableViewDataSource

//------------------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.topicList valueForKey:@"topics"] count];
}

//------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSSet *topics = [self.topicList valueForKey:@"topics"];
    
    NSManagedObject *topic = [[topics allObjects] objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopicCell"];
    cell.textLabel.text = [topic valueForKey:@"content"];
    
    cell.detailTextLabel.text = nil;
    NSDictionary *attributes = topic.entity.attributesByName;

    // Model 2: timeBudget units are minutes.
    if (nil != [attributes valueForKey:@"timeBudget"])
    {
        NSNumber *timeBudget = [topic valueForKey:@"timeBudget"];
        if (nil != timeBudget)
        {
            double timeInMinutes = [timeBudget doubleValue];
            if (timeInMinutes > 1)
            {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1fm", timeInMinutes];
            }
            else if (timeInMinutes > 0)
            {
                double timeInSeconds = ceil([timeBudget doubleValue]);
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%.0fs", timeInSeconds];
            }
        }
    }

    // Model 3: timeBudget units are seconds.
//    if (nil != [attributes valueForKey:@"timeBudget"])
//    {
//        NSNumber *timeBudget = [topic valueForKey:@"timeBudget"];
//        if (nil != timeBudget)
//        {
//            double timeInMinutes = [timeBudget doubleValue] / 60;
//            if (timeInMinutes > 1)
//            {
//                cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1fm", timeInMinutes];
//            }
//            else if (timeInMinutes > 0)
//            {
//                double timeInSeconds = ceil([timeBudget doubleValue]);
//                cell.detailTextLabel.text = [NSString stringWithFormat:@"%.0fs", timeInSeconds];
//            }
//        }
//    }

    return cell;
}

//------------------------------------------------------------------------------

@end

//------------------------------------------------------------------------------
